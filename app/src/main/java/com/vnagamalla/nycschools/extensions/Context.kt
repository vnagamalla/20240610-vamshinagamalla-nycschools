package com.vnagamalla.nycschools.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri

/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Showing that we can use extensions as utils functions
 * */
fun Context.launchDialer(phoneNumber: String) =
    Intent(Intent.ACTION_DIAL).apply {
        data = Uri.parse("tel:${phoneNumber}")
    }.also {
        startActivity(it)
    }

/**
 * Source from https://developers.google.com/maps/documentation/urls/android-intents
 * */
fun Context.launchDirections(address: String) =
    Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=$address")).apply {
        setPackage("com.google.android.apps.maps")
    }.also {
        startActivity(it)
    }

fun Context.launchEmail(email: String) =
    Intent(Intent.ACTION_SEND).apply {
        type = "message/rfc822"
        putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
    }.also {
        startActivity(it)
    }

fun Context.launchWeb(link: String) {
    val httpLink = if (link.contains("https://")) {
        Uri.parse(link)
    } else {
        Uri.parse("https://$link")
    }
    Intent(Intent.ACTION_VIEW, httpLink).also {
        startActivity(it)
    }
}

fun Context.share(link: String) =
    Intent(Intent.ACTION_SEND).apply {
        putExtra(Intent.EXTRA_TEXT, link)
        type = "text/plain"
    }.also {
        startActivity(Intent.createChooser(it, null))
    }