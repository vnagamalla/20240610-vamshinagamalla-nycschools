package com.vnagamalla.nycschools.extensions

import com.squareup.moshi.Moshi

/*
* extension to transform an object to json
* */
inline fun <reified T> Moshi.objectToJson(data: T): String =
    adapter(T::class.java).toJson(data)

/*
* extension to transform json to an object
* */
inline fun <reified T> Moshi.jsonToObject(json: String): T? =
    adapter(T::class.java).fromJson(json)