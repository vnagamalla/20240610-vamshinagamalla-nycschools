package com.vnagamalla.nycschools.repo.nycsatscores

import com.vnagamalla.nycschools.data.SatScores

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
class SatScoresRepoImpl(private val apiClient: SatScoresApiClient): SatScoresRepo {

    override suspend fun getSatScores(schoolToken: String): List<SatScores> =
        apiClient.getSatScores(schoolToken)
}