package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.data.School

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Repository for new york schools
 * */
interface NewYorkRepo {

    suspend fun getSchools(): List<School>

    suspend fun getSchool(token: String): School?

    suspend fun search(term: String): List<School>
}