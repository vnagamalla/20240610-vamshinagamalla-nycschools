package com.vnagamalla.nycschools.repo.nycsatscores

import com.vnagamalla.nycschools.data.SatScores
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * API for fetching sat scores
 * */
interface SatScoresApi {

    @GET("/resource/f9bf-2cp4.json")
    suspend fun getSatScores(@Query("dbn") schoolToken: String): List<SatScores>
}