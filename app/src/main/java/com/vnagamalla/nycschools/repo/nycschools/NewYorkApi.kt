package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.data.School
import retrofit2.http.GET

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * API for new york schools
 * */
interface NewYorkApi {

    @GET("/resource/s3k6-pzi2.json")
    suspend fun getHighSchools(): List<School>
}