package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.data.School

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Makes network request, or a database request, based on the request.
 * */
class NewYorkRepoImpl(private val apiClient: NewYorkApiClient,
                      private val dbClient: NewYorkDbClient): NewYorkRepo {

    override suspend fun getSchools() =
        apiClient.getSchools()
            .also {
                dbClient.insertAll(it)
            }

    override suspend fun getSchool(token: String) =
        dbClient.getSchool(token)

    override suspend fun search(term: String): List<School> =
        dbClient.search(term)
}