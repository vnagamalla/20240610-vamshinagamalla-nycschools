package com.vnagamalla.nycschools.repo.nycsatscores

import com.vnagamalla.nycschools.data.SatScores

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
class SatScoresApiClient(private val api: SatScoresApi) {

    suspend fun getSatScores(schoolToken: String): List<SatScores> =
        api.getSatScores(schoolToken)
}