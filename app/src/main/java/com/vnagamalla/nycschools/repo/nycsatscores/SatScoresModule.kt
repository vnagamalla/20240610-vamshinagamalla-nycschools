package com.vnagamalla.nycschools.repo.nycsatscores

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Module responsible for initializing the sat scores api and its repo
 * */
@Module
@InstallIn(SingletonComponent::class)
class SatScoresModule {

    @Provides
    @Singleton
    fun provideRepo(apiClient: SatScoresApiClient): SatScoresRepo =
        SatScoresRepoImpl(apiClient)

    @Provides
    @Singleton
    fun provideApiClient(api: SatScoresApi): SatScoresApiClient =
        SatScoresApiClient(api)

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): SatScoresApi =
        retrofit.create(SatScoresApi::class.java)
}