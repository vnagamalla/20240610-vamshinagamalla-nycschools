package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.database.CustomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Module responsible for initializing the NYCSchools api and its repo
 * */
@Module
@InstallIn(SingletonComponent::class)
class NewYorkModule {

    @Provides
    @Singleton
    fun provideApiRepo(apiClient: NewYorkApiClient,
                       dbClient: NewYorkDbClient): NewYorkRepo =
        NewYorkRepoImpl(apiClient, dbClient)

    @Provides
    @Singleton
    fun provideDbClient(database: CustomDatabase): NewYorkDbClient =
        NewYorkDbClient(database)

    @Provides
    @Singleton
    fun provideApiClient(api: NewYorkApi): NewYorkApiClient =
        NewYorkApiClient(api)

    @Provides
    @Singleton
    fun provideApi(retrofit: Retrofit): NewYorkApi =
        retrofit.create(NewYorkApi::class.java)
}