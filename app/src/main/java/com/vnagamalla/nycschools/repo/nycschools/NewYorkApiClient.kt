package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.data.School

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
class NewYorkApiClient(private val api: NewYorkApi) {

    suspend fun getSchools(): List<School> =
        api.getHighSchools()
}