package com.vnagamalla.nycschools.repo.nycschools

import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.database.CustomDatabase

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
class NewYorkDbClient(val database: CustomDatabase) {

    fun insertAll(schools: List<School>) =
        database.getSchoolsDao().insertAll(schools)

    fun getSchool(token: String) =
        database.getSchoolsDao().getSchoolForToken(token)

    fun search(term: String) =
        database.getSchoolsDao().search(term)
}