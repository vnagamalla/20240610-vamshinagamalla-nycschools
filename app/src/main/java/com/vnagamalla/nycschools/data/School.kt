package com.vnagamalla.nycschools.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
/**
 * Data class representing the school and its various details
 * */
@Entity(tableName = TableNames.SCHOOL_TABLE_NAME)
@JsonClass(generateAdapter = true)
data class School(
    @PrimaryKey
    @ColumnInfo(name = ColumnNames.School.TOKEN)
    @Json(name = "dbn")
    val token: String,

    @ColumnInfo(name = ColumnNames.School.NAME)
    @Json(name = "school_name")
    val name: String?,

    @ColumnInfo(name = ColumnNames.School.DESCRIPTION)
    @Json(name = "overview_paragraph")
    val description: String?,

    @ColumnInfo(name = ColumnNames.School.LOCATION)
    @Json(name = "location")
    val location: String?,

    @ColumnInfo(name = ColumnNames.School.LATITUDE)
    @Json(name = "latitude") val latitude: String?,

    @ColumnInfo(name = ColumnNames.School.LONGITUDE)
    @Json(name = "longitude")
    val longitude: String?,

    @ColumnInfo(name = ColumnNames.School.PHONE_NUMBER)
    @Json(name = "phone_number")
    val phoneNumber: String?,

    @ColumnInfo(name = ColumnNames.School.FAX_NUMBER)
    @Json(name = "fax_number")
    val faxNumber: String?,

    @ColumnInfo(name = ColumnNames.School.EMAIL)
    @Json(name = "school_email")
    val email: String?,

    @ColumnInfo(name = ColumnNames.School.WEBSITE)
    @Json(name = "website")
    val website: String?,

    @ColumnInfo(name = ColumnNames.School.SUBWAY)
    @Json(name = "subway")
    val subway: String?,

    @ColumnInfo(name = ColumnNames.School.BUS)
    @Json(name = "bus")
    val bus: String?,

    @ColumnInfo(name = ColumnNames.School.TOTAL_STUDENTS)
    @Json(name = "total_students")
    val totalStudents: String?
)

/**
 * TODO: wrap this differently, instead of having generic objects for table names and column names
 * */
object TableNames {

    const val SCHOOL_TABLE_NAME = "school"
}

object ColumnNames {

    object School {

        const val TOKEN = "token"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val LOCATION = "location"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val PHONE_NUMBER = "phone_number"
        const val FAX_NUMBER = "fax_number"
        const val EMAIL = "email"
        const val WEBSITE = "website"
        const val SUBWAY = "subway"
        const val BUS = "bus"
        const val TOTAL_STUDENTS = "total_students"
    }
}