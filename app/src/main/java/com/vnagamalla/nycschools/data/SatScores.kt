package com.vnagamalla.nycschools.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
/**
 * Data class representing the average sat scores of a school
 * */
@JsonClass(generateAdapter = true)
data class SatScores(
    @Json(name = "dbn")
    val token: String,
    @Json(name = "num_of_sat_test_takers")
    val noOfSatTestTakers: String?,
    @Json(name = "sat_critical_reading_avg_score")
    val criticalReadingAverageScore: String?,
    @Json(name = "sat_math_avg_score")
    val mathAverageScore: String?,
    @Json(name = "sat_writing_avg_score")
    val writingAverageScore: String?,
    @Json(name = "school_name")
    val schoolName: String?
)