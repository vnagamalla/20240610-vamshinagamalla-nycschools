package com.vnagamalla.nycschools.ui

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * This file contains the helper functions to prevent double tapping on an action item
 */
internal interface MultipleTapsCutter {
    fun processTap(event: () -> Unit)

    companion object
}

internal fun MultipleTapsCutter.Companion.get(): MultipleTapsCutter =
    MultipleTapsCutterImpl()

private class MultipleTapsCutterImpl : MultipleTapsCutter {
    private val now: Long
        get() = System.currentTimeMillis()

    private var lastEventTimeMs: Long = 0

    override fun processTap(event: () -> Unit) {
        if (now - lastEventTimeMs >= 300L) {
            event.invoke()
        }
        lastEventTimeMs = now
    }
}