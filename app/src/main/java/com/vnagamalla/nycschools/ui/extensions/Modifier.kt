package com.vnagamalla.nycschools.ui.extensions

import android.annotation.SuppressLint
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.platform.debugInspectorInfo
import androidx.compose.ui.semantics.Role
import com.vnagamalla.nycschools.ui.MultipleTapsCutter
import com.vnagamalla.nycschools.ui.get

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 */
/**
 * With the help of MultipleTapsCutter, this is a extension function for modifier to get a on click
 * callback just once.
 * */
@SuppressLint("ModifierFactoryUnreferencedReceiver")
fun Modifier.clickableSingle(
    enabled: Boolean = true,
    onClickLabel: String? = null,
    role: Role? = null,
    showIndication: Boolean = true,
    onClick: () -> Unit
) = composed(
    inspectorInfo = debugInspectorInfo {
        name = "clickable"
        properties["enabled"] = enabled
        properties["onClickLabel"] = onClickLabel
        properties["role"] = role
        properties["onClick"] = onClick
    }
) {
    val multipleTapsCutter = remember { MultipleTapsCutter.get() }
    Modifier.clickable(
        enabled = enabled,
        onClickLabel = onClickLabel,
        onClick = { multipleTapsCutter.processTap { onClick() } },
        role = role,
        indication = if (showIndication) LocalIndication.current else null,
        interactionSource = remember { MutableInteractionSource() }
    )
}