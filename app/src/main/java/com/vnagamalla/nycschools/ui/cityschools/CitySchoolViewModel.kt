package com.vnagamalla.nycschools.ui.cityschools

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepo
import com.vnagamalla.nycschools.ui.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
@HiltViewModel
class CitySchoolViewModel @Inject constructor(
    private val newYorkRepo: NewYorkRepo
): BaseViewModel() {

    val schoolList = MutableLiveData<List<School>>()

    init {
        retrieveSchools()
    }

    /**
     * retrieves the schools info from the API
     * */
    private fun retrieveSchools() {
        viewModelScope.launch(backgroundDispatcher) {
            try {
                schoolList.postValue(newYorkRepo.getSchools())
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }

    /**
     * searches for the school with @param term from database
     * */
    fun search(term: String) {
        viewModelScope.launch(backgroundDispatcher) {
            try {
                val list = newYorkRepo.search(term)
                schoolList.postValue(list)
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }
}