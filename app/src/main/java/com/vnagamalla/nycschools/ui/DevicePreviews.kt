package com.vnagamalla.nycschools.ui

import android.content.res.Configuration
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Helper annotation for various device previews in different combinations
 */
@PhoneLightPreview
@PhoneDarkPreview
@TabletLightPreview
@TabletDarkPreview
annotation class DevicePreviews

@PhoneLightPreview
@TabletLightPreview
annotation class LightModePreview

@PhoneDarkPreview
@TabletDarkPreview
annotation class DarkModePreview

@PhoneLightPreview
@PhoneDarkPreview
annotation class PhonePreview

@TabletLightPreview
@TabletDarkPreview
annotation class TabletPreview

@Preview(
        name = "phone light mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_NO,
        device = Devices.PIXEL_4_XL
)
annotation class PhoneLightPreview

@Preview(
        name = "phone dark mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_YES,
        device = Devices.PIXEL_4_XL
)
annotation class PhoneDarkPreview

@Preview(
        name = "tablet light mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_NO,
        device = Devices.PIXEL_C
)
annotation class TabletLightPreview

@Preview(
        name = "tablet dark mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_YES,
        device = Devices.PIXEL_C
)
annotation class TabletDarkPreview