package com.vnagamalla.nycschools.ui

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.ui.extensions.clickableSingle

/**
 * Search field can be used any where in the app, so extracted the code to reuse it
 * */
@Composable
fun CustomSearchField(search: (String) -> Unit) {

    var text by remember { mutableStateOf("") }



    TextField(
        value = text,
        onValueChange = {
            text = it
            search.invoke(it)
        },
        modifier = Modifier
            .testTag("Search")
            .fillMaxWidth()
            .padding(start = 15.dp, end = 15.dp)
            .scrollable(
                orientation = Orientation.Horizontal,
                state = rememberScrollState()
            ),
        textStyle = LocalTextStyle.current.copy(textAlign = TextAlign.Start),
        placeholder = {
            Text(
                text = stringResource(id = R.string.search),
                color = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.65f),
            )
        },
        leadingIcon = {
            Icon(
                painter = painterResource(id = R.drawable.magnifying_glass_small),
                contentDescription = "",
                tint = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.65f),
                modifier = Modifier.size(24.dp)
            )
        },
        trailingIcon = {
            if (text.isNotEmpty()) {
                Icon(
                    painter = painterResource(id = R.drawable.clear_small),
                    contentDescription = "Clear",
                    modifier = Modifier.size(28.dp)
                        .clickableSingle {
                            text = ""
                            search.invoke("")
                        },
                    tint = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.65f)
                )
            }
        },
        maxLines = 1,
        colors = TextFieldDefaults.colors(
            focusedContainerColor = MaterialTheme.colorScheme.primaryContainer,
            unfocusedContainerColor = MaterialTheme.colorScheme.primaryContainer,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent
        ),
        shape = RoundedCornerShape(20.dp)
    )
}
