package com.vnagamalla.nycschools.ui

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.vnagamalla.nycschools.logging.LogHelper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
open class BaseViewModel(var backgroundDispatcher: CoroutineDispatcher = Dispatchers.IO): ViewModel() {

    val error = mutableStateOf("")

    /**
     * Given more time, I would case out the connectivity errors and other errors, showing
     * specific error messages for connectivity and other errors.
     * */
    open fun handleException(e: Exception) {
        LogHelper.e(e)
        e.message?.let {
            error.value = it
        }
    }
}