package com.vnagamalla.nycschools.ui.cityschools

import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.extensions.launchDialer
import com.vnagamalla.nycschools.extensions.launchDirections
import com.vnagamalla.nycschools.extensions.launchEmail
import com.vnagamalla.nycschools.extensions.launchWeb
import com.vnagamalla.nycschools.extensions.share
import com.vnagamalla.nycschools.navigation.BottomNavScreen
import com.vnagamalla.nycschools.ui.CustomSearchField
import com.vnagamalla.nycschools.ui.extensions.clickableSingle
import com.vnagamalla.nycschools.ui.theme.NYCSchoolsTheme
import com.vnagamalla.nycschools.utils.Utils

/**
 * Screen representing the schools in a list.
 * */
@Composable
fun CitySchoolListScreen(
    navController: NavHostController,
    viewModel: CitySchoolViewModel = hiltViewModel()
) {

    val schoolList = viewModel.schoolList.observeAsState()

    NYCSchoolsTheme {
        Column(
            modifier = Modifier
                .testTag("School List")
        ) {

            SchoolListTopAppBar("City School List")
            Spacer(modifier = Modifier.height(16.dp))

            CustomSearchField {//search field
                viewModel.search(it)
            }

            Spacer(modifier = Modifier.height(5.dp))

            if (viewModel.error.value.isNotEmpty()) {
                Text(text = viewModel.error.value)
            } else if (schoolList.value.isNullOrEmpty()) {
                //showing loading message when fetching the data.
                LoadingState(
                    modifier = Modifier
                       .weight(1f)
                )

            } else {
                //representing the list

                schoolList.value?.let { schools ->
                    LazyColumnSchool(
                        schools = schools
                    ){
                        navController.navigate(BottomNavScreen.Detail.passData(it.token))
                    }
                }
            }
        }
    }
}



//representing the list
@Composable
fun LazyColumnSchool(
    schools: List<School>,
    onClick: (School) -> Unit
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(12.dp),
        modifier = Modifier.testTag("SchoolList")
    ) {
        items(schools.size) { index ->
            val school = schools[index]
            School(
                school,
            ) {
                onClick(school)
            }
        }
    }
}

@Composable
fun LoadingState(
    modifier: Modifier
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        CircularProgressIndicator(
            color = Color(0XFF2E7D32),
            strokeWidth = 4.dp,
            modifier = Modifier.size(40.dp)
        )
        Text(
            text = stringResource(id = R.string.fetching_schools),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp)
                .testTag("Loading"),
            textAlign = TextAlign.Center
        )
    }
}

/**
 * This composable represents a given school
 * */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun School(
    school: School,
    onClick: (School) -> Unit
) {
    val context = LocalContext.current

    Card(
        onClick = { onClick.invoke(school) },
        modifier = Modifier
            .fillMaxWidth()
            .testTag("School"),
        shape = RoundedCornerShape(12.dp),
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
    ) {
        Column(modifier = Modifier
            .fillMaxWidth()
            .clickableSingle {
                onClick.invoke(school)
            }
            .padding(start = 20.dp, end = 20.dp, top = 20.dp, bottom = 5.dp)
        ) {

            //school name
            Text(
                text = school.name!!,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.95f)
            )

            Spacer(modifier = Modifier.height(5.dp))

            //school location, clicking on this text will launch directions
            school.location?.let {
                Row(modifier = Modifier
                    .testTag("Direction")
                    .clickableSingle {
                        context.launchDirections(Utils.removeBracesAndContents(it))//removing latitude and longitude from data
                    }) {
                    Icon(
                        painter = painterResource(id = R.drawable.location),
                        contentDescription = "", modifier = Modifier.size(22.dp),
                        tint = Color(0XFF799351)
                    )
                    Spacer(modifier = Modifier.width(5.dp))
                    Text(
                        text = Utils.removeBracesAndContents(school.location),
                        color = MaterialTheme.colorScheme.onPrimaryContainer.copy(0.75f)
                    )
                }
            }

            Spacer(modifier = Modifier.height(5.dp))

            //quick share options
            SchoolOptions(school)
        }
    }


}


/**
 * Helper function to represent all action items for a given school
 *
 * Favoured aesthetics, instead of removing the icon when some data is not available, showing the icon
 * and disabling it, and showing a toast if a data is not available
 * */
@Composable
fun SchoolOptions(school: School) {
    val context = LocalContext.current
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        MenuOption(
            icon = R.drawable.phone_outline_small,
            isEnabled = !school.phoneNumber.isNullOrEmpty(),
            contentDescription = "Phone"
        ) {
            if (school.phoneNumber == null) {
                Toast.makeText(context, R.string.phone_number_not_available, Toast.LENGTH_SHORT).show()
            } else {
                context.launchDialer(school.phoneNumber)
            }
        }
        MenuOption(
            icon = R.drawable.globe_small,
            isEnabled = !school.website.isNullOrEmpty(),
            contentDescription = "Website"
        ) {
            if (school.website == null) {
                Toast.makeText(context, R.string.phone_website_not_available, Toast.LENGTH_SHORT).show()
            } else {
                context.launchWeb(school.website)
            }
        }
        MenuOption(
            icon = R.drawable.email_small,
            isEnabled = !school.email.isNullOrEmpty(),
            contentDescription = "Email"
        ) {
            if (school.email == null) {
                Toast.makeText(context, R.string.phone_email_not_available, Toast.LENGTH_SHORT).show()
            } else {
                context.launchEmail(school.email)
            }
        }
        MenuOption(
            icon = R.drawable.share_nodes_small,
            isEnabled = !school.website.isNullOrEmpty(),
            contentDescription = "Share"
        ) {
            if (school.website == null) {
                Toast.makeText(context, R.string.phone_website_not_available, Toast.LENGTH_SHORT).show()
            } else {
                context.share(school.website)
            }
        }
    }
}

/**
 * Helper function representing an action item.
 * */
@Composable
fun MenuOption(
    @DrawableRes icon: Int,
    isEnabled: Boolean = false,
    contentDescription : String ,
    onClick: () -> Unit,
) {
    Box(
        modifier = Modifier
            .size(48.dp)
            .testTag("Menu Option")
            .clickableSingle {
                onClick.invoke()
            }
            .background(MaterialTheme.colorScheme.tertiaryContainer, CircleShape)
            .padding(8.dp)
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = contentDescription, tint = if (isEnabled) {
                MaterialTheme.colorScheme.onPrimaryContainer
            } else {
                MaterialTheme.colorScheme.onPrimaryContainer.copy(0.5f)
            },

        )
    }
}



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolListTopAppBar(
    title :String
) {

    CenterAlignedTopAppBar(
        title = {
            Text(
                text = title,
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onSecondaryContainer
            )
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primary
        )
    )

}