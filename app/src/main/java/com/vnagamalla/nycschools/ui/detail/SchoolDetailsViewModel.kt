package com.vnagamalla.nycschools.ui.detail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vnagamalla.nycschools.data.SatScores
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepo
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepo
import com.vnagamalla.nycschools.ui.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(private val newYorkRepo: NewYorkRepo,
                                                 private val satScoresRepo: SatScoresRepo): BaseViewModel() {

    val school = MutableLiveData<School>()
    val satScores = MutableLiveData<SatScores?>()

    fun retrieveSatScores(token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                val result = satScoresRepo.getSatScores(token)
                if (result.isEmpty()) {
                    satScores.postValue(null)
                } else {
                    satScores.postValue(result[0])
                }
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }

    fun retrieveSchool(token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                school.postValue(newYorkRepo.getSchool(token))
            } catch (e: Exception) {
                handleException(e)
            }
        }
    }
}