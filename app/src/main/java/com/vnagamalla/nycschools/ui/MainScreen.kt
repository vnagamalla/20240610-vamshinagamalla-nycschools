package com.vnagamalla.nycschools.ui

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.vnagamalla.nycschools.navigation.BottomNavScreen
import com.vnagamalla.nycschools.navigation.NavGraph

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Responsible for initiating the main screen of the app.
 * */
@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun MainScreen(navController: NavHostController) {

    Scaffold(
        bottomBar = {
            BottomBar1(navController = navController)
                    },
        content = {
            NavGraph(navHostController = navController)
        })
}

@Composable
fun BottomBar1(navController: NavHostController) {
    val screens = listOf(
        BottomNavScreen.Home,
        BottomNavScreen.Profile,
    )
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    NavigationBar(
        containerColor = MaterialTheme.colorScheme.primary,
        modifier = Modifier
            .fillMaxWidth()
            .testTag("Bottom Navigation")
    ) {
        screens.forEach { screens ->
            if (currentDestination != null) {
                AddItem1(
                    screen = screens,
                    currentDestination = currentDestination,
                    navController = navController
                )
            }
        }
    }
}

@Composable
fun RowScope.AddItem1(
    screen: BottomNavScreen,
    currentDestination: NavDestination,
    navController: NavHostController
) {
    NavigationBarItem(
        selected = currentDestination.hierarchy.any {
            it.route == screen.route
        },
        onClick = {
            navController.navigate(route = screen.route) {
                popUpTo(navController.graph.id)
                launchSingleTop = true
            }
        },
        enabled = true,
        icon = {
            Icon(
                painter = painterResource(id = screen.icon),
                contentDescription = screen.route,
                modifier = Modifier.size(28.dp)
            )
               },
    //    label = { Text(text = screen.title) },
        colors = NavigationBarItemDefaults.colors(
            selectedIconColor = MaterialTheme.colorScheme.onSecondaryContainer,
            selectedTextColor = MaterialTheme.colorScheme.onSecondaryContainer,
            unselectedIconColor = MaterialTheme.colorScheme.onSecondaryContainer.copy(0.4f),
            unselectedTextColor = MaterialTheme.colorScheme.onSecondaryContainer.copy(0.4f),
            indicatorColor = MaterialTheme.colorScheme.primary

        ),
        modifier = Modifier.semantics {
            contentDescription = screen.route
        }
    )

}


@Composable
fun BottomBar(navController: NavHostController) {
    val screens = listOf(
        BottomNavScreen.Home,
        BottomNavScreen.Profile,
    )
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    NavigationBar {
        screens.forEach { screens ->
            if (currentDestination != null) {
                AddItem(
                    screen = screens,
                    currentDestination = currentDestination,
                    navController = navController
                )
            }
        }
    }
}

@Composable
fun RowScope.AddItem(
    screen: BottomNavScreen,
    currentDestination: NavDestination,
    navController: NavHostController
) {
    NavigationBarItem(
        selected = currentDestination.hierarchy.any {
            it.route == screen.route
        },
        onClick = {
            navController.navigate(route = screen.route) {
                popUpTo(navController.graph.id)
                launchSingleTop = true
            }
        },
        icon = {
            Image(
                painter = painterResource(screen.icon),
                contentDescription = screen.route,

            )
               },
        label = { Text(text = screen.title) }
    )

}
