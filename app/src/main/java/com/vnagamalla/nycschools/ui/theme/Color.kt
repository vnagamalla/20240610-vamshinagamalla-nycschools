package com.vnagamalla.nycschools.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val Green80 = Color(0xFF254922)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val Green40 = Color(0xFF57A450)
val Green10 = Color(0xFFDDEDDC)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val White = Color(0xFFFFFFFF)
val Black = Color(0xFF000000)


//Dark Mode
val Green20 = Color(0XFFA1DD70).copy(0.2f)
val Blue = Color(0XFF5F50B9).copy(0.1f)
val Black3 = Color(0XFF383443)


