package com.vnagamalla.nycschools.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.extensions.launchWeb
import com.vnagamalla.nycschools.ui.cityschools.SchoolListTopAppBar
import com.vnagamalla.nycschools.ui.extensions.clickableSingle
import com.vnagamalla.nycschools.ui.theme.NYCSchoolsTheme

/**
 * Author info, clickable profile name directs to linkedin
 * */
@Composable
fun ProfileScreen() {
    val context = LocalContext.current
    NYCSchoolsTheme {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            SchoolListTopAppBar(title = "Profile")
            Spacer(modifier = Modifier.height(100.dp))
            Image(
                painter = painterResource(id = R.drawable.avator),
                contentDescription = null ,
                modifier = Modifier
                    .size(120.dp)
                    .clip(CircleShape)
            )
            Text(text = "Author",
                modifier = Modifier.fillMaxWidth(),
                fontSize = 28.sp,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onPrimaryContainer
            )
            Spacer(modifier = Modifier.height(10.dp))
            Row(modifier = Modifier
                .fillMaxWidth()
                .clickableSingle {
                    context.launchWeb("https://www.linkedin.com/in/vamshinagamalla/")
                }
                .padding(top = 5.dp, bottom = 5.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center) {
                Icon(
                    painter = painterResource(id = R.drawable.linkedin),
                    contentDescription = "",
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colorScheme.onPrimaryContainer
                )
                Spacer(modifier = Modifier.width(5.dp))
                Text(
                    text = "Vamshi Nagamalla",
                    fontWeight = FontWeight.Medium,
                    color = MaterialTheme.colorScheme.onPrimaryContainer
                )
            }
            Spacer(modifier = Modifier.height(40.dp))
        }
    }
}