package com.vnagamalla.nycschools.ui.detail

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.Marker
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.data.SatScores
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.extensions.launchDialer
import com.vnagamalla.nycschools.extensions.launchDirections
import com.vnagamalla.nycschools.extensions.launchEmail
import com.vnagamalla.nycschools.extensions.launchWeb
import com.vnagamalla.nycschools.ui.extensions.clickableSingle
import com.vnagamalla.nycschools.ui.theme.NYCSchoolsTheme
import com.vnagamalla.nycschools.utils.Utils

/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SchoolDetailsScreen(
    navHostController: NavHostController,
    viewModel: SchoolDetailsViewModel = hiltViewModel(),
    token: String?
) {
    val context = LocalContext.current

    val school = viewModel.school.observeAsState()
    val satScores = viewModel.satScores.observeAsState()

    token?.let {
        LaunchedEffect(true) {
            viewModel.retrieveSatScores(it)
            viewModel.retrieveSchool(it)
        }
    }


    NYCSchoolsTheme {
        Column {
            //showing the title of the screen

            TopAppBar(
                title = {
                    Text(
                        text = school.value?.name ?: "",
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        color = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                    },
                navigationIcon = {
                    Icon(
                        painter = painterResource(id = R.drawable.arrow_left_small),
                        contentDescription = "Back Press",
                        modifier = Modifier
                            .size(44.dp)
                            .padding(8.dp)
                            .clickableSingle {
                                navHostController.popBackStack()
                            },
                        tint = MaterialTheme.colorScheme.onSecondaryContainer
                    )
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary
                )

            )

            //showing the map marker, since no data on the image.
            school.value?.let { school ->
                school.latitude?.let { latitude ->
                    school.longitude?.let { longitude ->
                        SchoolMap(school = school, latitude,latitude)
                    }
                }
            }

            Spacer(modifier = Modifier.height(5.dp))
            Column(modifier = Modifier
                .fillMaxWidth()
                .testTag("DetailScreen")
                .padding(start = 20.dp, top = 10.dp, end = 20.dp, bottom = 100.dp)
                .verticalScroll(state = rememberScrollState()),
                verticalArrangement = Arrangement.spacedBy(5.dp)) {

                school.value?.let { school ->

                    //school name
                    school.name?.let {
                        Text(
                            text = it,
                            fontWeight = FontWeight.Bold,
                            fontSize = 22.sp,
                            color = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                        Spacer(modifier = Modifier.height(5.dp))
                    }

                    //sat scores if available
                    if (viewModel.error.value.isNotEmpty()) {
                        //given more time I will show this error in a separate dialog
                        Text(
                            text = viewModel.error.value,
                            color = MaterialTheme.colorScheme.onPrimaryContainer,
                            modifier = Modifier.testTag("Error")
                        )
                    } else if (viewModel.satScores.value == null) {
                        Text(
                            text = stringResource(id = R.string.sat_scores_are_not_available_for_this_school),
                            color = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    } else {
                        SatScoresBox(
                            satScores = satScores.value,
                            school = school
                        )
                    }

                    //school overview
                    school.description?.let {
                        Spacer(modifier = Modifier.height(10.dp))
                        Text(
                            text = stringResource(id = R.string.about_this_school),
                            fontWeight = FontWeight.Bold,
                            color = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                        Text(
                            text = it,
                            fontWeight = FontWeight.Normal,
                            color = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    }

                    Spacer(modifier = Modifier.height(5.dp))

                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(
                                color = MaterialTheme.colorScheme.primaryContainer,
                                RoundedCornerShape(12.dp)
                            )
                            .clip(RoundedCornerShape(12.dp))
                            .padding(12.dp)
                    ){
                        Column {
                            //school location
                            school.location?.let {
                                val address = Utils.removeBracesAndContents(it)
                                SchoolInfo(
                                    text = address,
                                    icon = R.drawable.directions_small
                                ) {
                                    context.launchDirections(address)
                                }
                            }

                            //school phone number
                            school.phoneNumber?.let {
                                SchoolInfo(text = it, icon = R.drawable.phone_outline_small) {
                                    context.launchDialer(it)
                                }
                            }

                            //school email
                            school.email?.let {
                                SchoolInfo(text = it, icon = R.drawable.email_small) {
                                    context.launchEmail(it)
                                }
                            }

                            //school website
                            school.website?.let {
                                SchoolInfo(
                                    text = it,
                                    icon = R.drawable.globe_small
                                ) {
                                    context.launchWeb(it)
                                }
                            }

                            //school fax number
                            school.faxNumber?.let {
                                Text(
                                    text = stringResource(id = R.string.fax, it),
                                    fontWeight = FontWeight.Medium,
                                    fontSize = 14.sp
                                )
                            }

                            //subway
                            school.subway?.let {
                                Text(
                                    text = stringResource(id = R.string.subway, it),
                                    fontWeight = FontWeight.Medium,
                                    fontSize = 14.sp
                                )
                            }

                            //bus
                            school.bus?.let {
                                Text(
                                    text = stringResource(id = R.string.bus, it),
                                    fontWeight = FontWeight.Medium,
                                    fontSize = 14.sp
                                )
                            }
                        }

                    }


                }
            }
        }
    }
}



@Composable
fun SatScoresBox(
    satScores: SatScores?,
    school: School?
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .testTag("SatScores")
            .background(
                MaterialTheme.colorScheme.primaryContainer,
                RoundedCornerShape(12.dp)
            )
            .clip(RoundedCornerShape(12.dp))
            .padding(12.dp)
    ){
        Column {
            satScores?.let { scores ->
                scores.noOfSatTestTakers?.let { testTakers ->
                    school?.totalStudents?.let {
                        //percentage of test takers
                        Text(
                            text = stringResource(id = R.string.percentage_of_test_takers,
                                Utils.calculatePercentage(testTakers.toInt(), it.toInt())),
                            fontWeight = FontWeight.Medium,
                            color = MaterialTheme.colorScheme.onPrimaryContainer
                        )
                    }
                }
                //critical reading
                scores.criticalReadingAverageScore?.let {
                    Text(
                        text = stringResource(
                            id = R.string.average_critical_reading_score,
                            it
                        ), fontWeight = FontWeight.Medium,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                }
                //math
                scores.mathAverageScore?.let {
                    Text(
                        text = stringResource(
                            id = R.string.average_math_score,
                            it
                        ),
                        fontWeight = FontWeight.Medium,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                }
                //writing
                scores.writingAverageScore?.let {
                    Text(
                        text = stringResource(
                            id = R.string.average_writing_score,
                            it
                        ),
                        fontWeight = FontWeight.Medium,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                }
            }
        }
    }
}

/**
 * Composable representing info of the school with given icon and text next to icon
 * */
@Composable
fun SchoolInfo(text: String,
               @DrawableRes icon: Int,
               onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .clickableSingle {
                onClick.invoke()
            }
            .padding(top = 5.dp, bottom = 5.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            painter = painterResource(id = icon),
            contentDescription = "",
            modifier = Modifier.size(28.dp),
            tint = MaterialTheme.colorScheme.onTertiaryContainer
        )
        Spacer(
            modifier = Modifier.width(5.dp)
        )
        Text(
            text = text,
            fontSize = 14.sp,
            fontWeight = FontWeight.Medium,
            color = MaterialTheme.colorScheme.onPrimaryContainer
        )
    }
}


/**
 * Composable showing the marker on the map of the school
 * */
@Composable
fun SchoolMap(school: School, latitude: String, longitude: String) {
    val cameraPositionState = rememberCameraPositionState {
        position = CameraPosition.fromLatLngZoom(LatLng(latitude.toDouble(), longitude.toDouble()), 13f)
    }

    GoogleMap(
        modifier = Modifier
            .fillMaxWidth()
            .height(200.dp), cameraPositionState = cameraPositionState,
        uiSettings = MapUiSettings()
    ) {
        Marker(
            state = MarkerState(
                position = LatLng(
                    latitude.toDouble(),
                    longitude.toDouble()
                )
            ), title = school.name, snippet = school.location
        )
    }
}