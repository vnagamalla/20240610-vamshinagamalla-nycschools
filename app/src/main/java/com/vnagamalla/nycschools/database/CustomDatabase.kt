package com.vnagamalla.nycschools.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vnagamalla.nycschools.data.School

/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Note: version is hardcoded, but in a real environment the versions will defined gradle
 * */
@Database(entities = [School::class], version = 1)
abstract class CustomDatabase: RoomDatabase() {

    abstract fun getSchoolsDao(): SchoolDao
}