package com.vnagamalla.nycschools.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.vnagamalla.nycschools.data.ColumnNames
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.data.TableNames


/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
@Dao
abstract class SchoolDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(members: List<School>): LongArray?

    @Query("SELECT * FROM ${TableNames.SCHOOL_TABLE_NAME} WHERE ${ColumnNames.School.TOKEN} IN (:token)")
    abstract fun getSchoolForToken(token: String): School?

    /**
     * Description is not shown on a list item, but querying from description as to show case that
     * it can be done in a single call
     * */
    @Query("SELECT * FROM ${TableNames.SCHOOL_TABLE_NAME} WHERE ${ColumnNames.School.NAME} LIKE '%' || :term || '%' " +
            "OR ${ColumnNames.School.DESCRIPTION} LIKE '%' || :term || '%'")
    abstract fun search(term: String): List<School>
}