package com.vnagamalla.nycschools.database

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Vamshi Nagamalla on 1/11/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Note:
 * 1. in a real environment, I would prefer to have the name of the table in a constant file or
 * anything related to app constants
 * 2. We could also consider using InMemory database, since this data needs to be refreshed
 * every time the app is loaded
 * */
@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(@ApplicationContext context: Context): CustomDatabase =
        Room.databaseBuilder(context, CustomDatabase::class.java, "NYCSchools")
            .fallbackToDestructiveMigration()
            .build()
}