package com.vnagamalla.nycschools.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.vnagamalla.nycschools.ui.cityschools.CitySchoolListScreen
import com.vnagamalla.nycschools.ui.cityschools.CitySchoolViewModel
import com.vnagamalla.nycschools.ui.detail.SchoolDetailsScreen
import com.vnagamalla.nycschools.ui.detail.SchoolDetailsViewModel
import com.vnagamalla.nycschools.ui.profile.ProfileScreen


/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Class representing the navigation routes
 * */
@Composable
fun NavGraph(navHostController: NavHostController) {

   // NavHO

    NavHost(navController = navHostController, startDestination = BottomNavScreen.Home.route) {
        composable(route = BottomNavScreen.Home.route) {
            CitySchoolListScreen(
                navHostController,
            //    hiltViewModel()
            )
        }
        composable(route = BottomNavScreen.Profile.route) {
            ProfileScreen()
        }
        composable(route = BottomNavScreen.Detail.route, arguments = listOf(
            navArgument(name = "token") {
                type = NavType.StringType
            },
        ),
            enterTransition = { //enter animation for school detail screen
            slideIntoContainer(towards = AnimatedContentTransitionScope.SlideDirection.Left,
                animationSpec = tween(700))
        },
            exitTransition = {//exit animation for school detail screen
            slideOutOfContainer(towards = AnimatedContentTransitionScope.SlideDirection.Right,
                animationSpec = tween(700))
        }
        ) {
            SchoolDetailsScreen(
                navHostController,
          //      hiltViewModel(),
               token =  it.arguments?.getString("token"))
        }
    }
}