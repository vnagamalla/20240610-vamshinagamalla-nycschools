package com.vnagamalla.nycschools.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.vnagamalla.nycschools.R

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Class representing the navigation areas of the app.
 * */
sealed class BottomNavScreen(
    val route: String,
    val title: String,
    val icon: Int
  //  val icon: ImageVector
) {
    data object Home : BottomNavScreen(
        route = "home",
        title = "Home",
        icon = R.drawable.home
    )

    data object Profile : BottomNavScreen(
        route = "profile",
        title = "Profile",
        icon = R.drawable.profile
    )

    data object Detail : BottomNavScreen(
        route = "detail/{token}",
        title = "Details",
        icon = R.drawable.more_vert
    ) {
        fun passData(
            token: String,
        ): String {
            return "detail/$token"
        }
    }
}