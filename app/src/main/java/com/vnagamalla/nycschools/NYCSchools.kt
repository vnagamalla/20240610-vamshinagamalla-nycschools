package com.vnagamalla.nycschools

import android.app.Application
import com.vnagamalla.nycschools.logging.DebugLogTree
import com.vnagamalla.nycschools.logging.LogFileProvider
import com.vnagamalla.nycschools.logging.LogHelper
import com.vnagamalla.nycschools.logging.LogUtils
import com.vnagamalla.nycschools.logging.ReleaseLogTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber


/**
 * Created by Vamshi Nagamalla on 1/8/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
@HiltAndroidApp
class NYCSchools: Application() {

    override fun onCreate() {
        super.onCreate()
        val logUtils = LogUtils()
        val logFileProvider = LogFileProvider(this, logUtils)

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugLogTree(logFileProvider, logUtils) as Timber.Tree)
            LogHelper.i("Debug Tree")
        } else {
            Timber.plant(ReleaseLogTree(logFileProvider, logUtils) as Timber.Tree)
        }
    }
}