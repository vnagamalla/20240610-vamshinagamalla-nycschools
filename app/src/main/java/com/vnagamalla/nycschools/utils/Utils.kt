package com.vnagamalla.nycschools.utils

object Utils {

    /**
     * Helper function to remove braces and it contents from a string
     * */
    fun removeBracesAndContents(string: String) =
        string.replace(Regex("\\([^)]*\\)"), "")

    /**
     * Helper function to calculate percentage up to two decimal points
     * */
    fun calculatePercentage(value: Int, total: Int): String {
        val percentage = (value.toDouble() / total) * 100
        return String.format("%.2f", percentage)
    }
}