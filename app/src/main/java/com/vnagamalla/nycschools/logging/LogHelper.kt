package com.vnagamalla.nycschools.logging

import timber.log.Timber

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Helper class providing all the log functions needed, not using every variant in this project.
 * */
internal object LogHelper {

    /** Log a verbose message with optional format args.  */
    @JvmStatic
    fun v(message: String?, vararg args: Any?) {
        Timber.v(message, *args)
    }

    /** Log a verbose exception and a message with optional format args.  */
    @JvmStatic
    fun v(t: Throwable?, message: String?, vararg args: Any?) {
        Timber.v(t, message, *args)
    }

    /** Log a verbose exception.  */
    @JvmStatic
    fun v(t: Throwable?) {
        Timber.v(t)
    }

    /** Log a debug message with optional format args.  */
    @JvmStatic
    fun d(message: String?, vararg args: Any?) {
        Timber.d(message, *args)
    }

    /** Log a debug exception and a message with optional format args.  */
    @JvmStatic
    fun d(t: Throwable?, message: String?, vararg args: Any?) {
        Timber.d(t, message, *args)
    }

    /** Log a debug exception.  */
    @JvmStatic
    fun d(t: Throwable?) {
        Timber.d(t)
    }

    /** Log an info message with optional format args.  */
    @JvmStatic
    fun i(message: String?, vararg args: Any?) {
        Timber.i(message, *args)
    }

    /** Log an info exception and a message with optional format args.  */
    @JvmStatic
    fun i(t: Throwable?, message: String?, vararg args: Any?) {
        Timber.i(t, message, *args)
    }

    /** Log an info exception.  */
    @JvmStatic
    fun i(t: Throwable?) {
        Timber.i(t)
    }

    /** Log a warning message with optional format args.  */
    @JvmStatic
    fun w(message: String?, vararg args: Any?) {
        Timber.w(message, *args)
    }

    /** Log a warning exception and a message with optional format args.  */
    @JvmStatic
    fun w(t: Throwable?, message: String?, vararg args: Any?) {
        Timber.w(t, message, *args)
    }

    /** Log a warning exception.  */
    @JvmStatic
    fun w(t: Throwable?) {
        Timber.w(t)
    }

    /** Log an error message with optional format args.  */
    @JvmStatic
    fun e(message: String?, vararg args: Any?) {
        Timber.e(message, *args)
    }

    @JvmStatic
    fun e(message: String?, t: Throwable) {
        Timber.e(t, message, null)
    }

    /** Log an error exception and a message with optional format args.  */
    @JvmStatic
    fun e(t: Throwable?, message: String?, vararg args: Any?) {
        Timber.e(t, message, *args)
    }

    /** Log an error exception.  */
    @JvmStatic
    fun e(t: Throwable?) {
        Timber.e(t)
    }

    /** Log at `priority` a message with optional format args.  */
    @JvmStatic
    fun log(priority: Int, message: String?, vararg args: Any?) {
        Timber.log(priority, message, *args)
    }

    /** Log at `priority` an exception and a message with optional format args.  */
    @JvmStatic
    fun log(priority: Int, t: Throwable?, message: String?, vararg args: Any?) {
        Timber.log(priority, t, message, *args)
    }

    /** Log at `priority` an exception.  */
    @JvmStatic
    fun log(priority: Int, t: Throwable?) {
        Timber.log(priority, t)
    }
}
