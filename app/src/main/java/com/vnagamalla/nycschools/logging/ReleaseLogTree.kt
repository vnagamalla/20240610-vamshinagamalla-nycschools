package com.vnagamalla.nycschools.logging

import android.util.Log
import com.vnagamalla.nycschools.BuildConfig


/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
class ReleaseLogTree(logFileProvider: LogFileProvider, logUtils: LogUtils): DebugLogTree(logFileProvider, logUtils) {

    /**
     * We could also consider logging in case of dev, staging and other non-prod environments
     * */
    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return if (priority == Log.DEBUG) {
            BuildConfig.DEBUG
        } else {
            super.isLoggable(tag, priority)
        }
    }
}
