package com.vnagamalla.nycschools.logging

import android.content.Context
import android.os.Build
import android.util.Log
import java.io.File
import java.io.FileWriter

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 * */
/**
 * Class responsible for creating, initiating, deleting log files in app memory.
 *
 * 1. Creates a log file if one is not there based on the time stamp
 * 2. Deletes the log file if there are more than 7
 * 3. Initializes the log file with device details
 * */
class LogFileProvider(private val context: Context, private val logFileUtils: LogUtils) {

    private val MAX_FILES_IN_DIR: Long = 7

    fun generateFile(fileNameDateStamp: String): File {

        val logsDirectory = File(context.getExternalFilesDir(null), "logs")

        if (!logsDirectory.exists()) {
            logsDirectory.mkdirs()
        }

        val fileName = "$fileNameDateStamp.txt"
        val logFile = File(logsDirectory, fileName)

        when {
            logFile.exists() -> return logFile
            logsDirectory.listFiles()?.size!! >= MAX_FILES_IN_DIR -> {
                logsDirectory.listFiles()?.sortedWith(compareBy { it.lastModified() })?.get(0)?.delete()
            }
        }

        initFileWithDeviceDetails(logFile)

        return logFile
    }

    private fun initFileWithDeviceDetails(file: File) {
        try {
            val writer = FileWriter(file, true)
            val logIdentifier = logFileUtils.getLogStatementIdentifier(Log.INFO)
            writer.append("${logFileUtils.getLogStatement()} $logIdentifier Brand ${Build.BRAND}")
                .append("\n")
                .append("${logFileUtils.getLogStatement()} $logIdentifier Device ${Build.DEVICE}")
                .append("\n")
                .append("${logFileUtils.getLogStatement()} $logIdentifier Model ${Build.MODEL}")
                .append("\n")
                .append("${logFileUtils.getLogStatement()} $logIdentifier Product ${Build.PRODUCT}")
            writer.flush()
            writer.close()

        } catch (e: Exception) {
            logFileUtils.logToLogCat(Log.ERROR, javaClass.name, "Exception while writing device details " + e.message, e)
        }
    }
}