package com.vnagamalla.nycschools.logging

import android.annotation.SuppressLint
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Class containing various helper functions needed for logs
 * */
class LogUtils {

    internal fun getLogStatementIdentifier(priority: Int): String {
        when (priority) {
            Log.ERROR -> return "E"
            Log.DEBUG -> return "D"
            Log.INFO -> return "I"
            Log.VERBOSE -> return "V"
            Log.WARN -> return "W"
            Log.ASSERT -> return "A"
        }

        return "None"
    }

    internal fun getLogStatement(): String {
        return SimpleDateFormat("E MMM dd yyyy 'at' hh:mm:ss:SSS aaa z",
                Locale.ENGLISH).format(Date())
    }

    @SuppressLint("LogNotTimber")
    internal fun logToLogCat(priority: Int?, tag: String?, message: String, t: Throwable?) {
        when(priority) {
            Log.DEBUG -> {
                if (t != null) Log.d(tag, message, t) else Log.d(tag, message)
            }
            Log.ERROR -> {
                if (t != null) Log.e(tag, message, t) else Log.e(tag, message)
            }
            Log.WARN -> {
                if (t != null) Log.w(tag, message, t) else Log.w(tag, message)
            }
            Log.INFO -> {
                if (t != null) Log.i(tag, message, t) else Log.i(tag, message)
            }
            Log.VERBOSE -> {
                if (t != null) Log.v(tag, message, t) else Log.v(tag, message)
            }
            Log.ASSERT -> {
                if (t != null) Log.wtf(tag, message, t) else Log.wtf(tag, message)
            }
        }
    }

    internal fun getSystemTime(): String {
        return SimpleDateFormat("MM-dd-yyyy", Locale.ENGLISH).format(Date())
    }
}