package com.vnagamalla.nycschools.logging

import android.annotation.SuppressLint
import android.util.Log
import timber.log.Timber
import java.io.File
import java.io.FileWriter


/**
 * Created by Vamshi Nagamalla on 1/10/24.
 * Copyright © Vamshi Nagamalla. All rights reserved.
 *
 * Log tree, responsible for creating log file in app memory, writes the logs into the file so that
 * when needed a user can email us the logs for an issue/support
 * */
open class DebugLogTree(private val logFileProvider: LogFileProvider,
                        private val logUtils: LogUtils): Timber.DebugTree() {

    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return true
    }

    @SuppressLint("LogNotTimber")
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {

        if (isLoggable(tag, priority)) {

            val logTimeStamp = logUtils.getLogStatement()

            //record message for crashlytics

            if (t != null) {

                if (priority == Log.DEBUG) {
                    t.printStackTrace()
                }

                //record crash for crashlytics
            }

            logUtils.logToLogCat(priority, "$logTimeStamp $tag", message, t)

            try {
                val file: File = logFileProvider.generateFile(logUtils.getSystemTime())

                val writer = FileWriter(file, true)
                writer.append("\n")
                        .append(logTimeStamp)
                        .append(" ")
                        .append(logUtils.getLogStatementIdentifier(priority))
                        .append(" ")
                        .append(tag)
                        .append(" ")
                        .append(message)
                writer.flush()
                writer.close()

            } catch (e: Exception) {
                logUtils.logToLogCat(priority, javaClass.name, message, e)
            }
        }
    }

    /**
     * Trying to generalize the log prefix, with file name, method name and line number
     *
     * Given more time, would love to solve the issue with obfuscation.
     * */
    override fun createStackElementTag(element: StackTraceElement): String {
        return try {
            val stackTrace = Throwable().stackTrace
            //fetching stacktrace at index 6, Timber is logging the stacktrace at 5, since we are adding one more layer
            //we bumped up the number by 1.
            "${stackTrace[6].fileName}#${stackTrace[6].methodName}:${stackTrace[6].lineNumber}"
        } catch (e: Exception) {
            e.printStackTrace()
            //record crash for crashlytics
            "unidentified stacktrace"
        }
    }
}