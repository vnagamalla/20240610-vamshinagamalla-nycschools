package com.vnagamalla.nycschools.database

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.vnagamalla.nycschools.ui.cityschools.SchoolList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test

class CustomDatabaseTest {


    private lateinit var db : CustomDatabase
    private lateinit var schoolDao : SchoolDao

    @Before
    fun setup(){
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context,CustomDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        schoolDao = db.getSchoolsDao()
    }

    @After
    fun tearUp(){
        db.close()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun insertSchool() = runTest(UnconfinedTestDispatcher()) {
        schoolDao.insertAll(SchoolList.schools)

        val school = schoolDao.getSchoolForToken("02M260")
        assert(SchoolList.schools[0] == school)


        val search = schoolDao.search("Clinton School")
        assert(search[0] == SchoolList.schools[0])


    }
}