package com.vnagamalla.nycschools.di

import android.app.Application
import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnitRunner
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.vnagamalla.nycschools.network.ApiModule
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit

class CustomTestRunner  : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return super.newApplication(cl, HiltTestApplication::class.java.name, context)
    }
}
@HiltAndroidTest

class ApiModuleTest {



    @get:Rule
    var hiltRule = HiltAndroidRule(this)



    private lateinit var apiModule: ApiModule



    private lateinit var httpClient: OkHttpClient


    private lateinit var context: Context
    private lateinit var moshi: Moshi



    @Before
    fun setUp() {
        hiltRule.inject()
        apiModule = ApiModule()

        context = InstrumentationRegistry.getInstrumentation().targetContext
        val loggingInterceptor = apiModule.provideLoggingInterceptor()
        val cache = Cache(context.cacheDir, (10 * 1024 * 1024).toLong())
        httpClient =   OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(loggingInterceptor)
            .cache(cache)
            .build()
        moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }



    @Test
    fun testProvideRetrofit() {
        val retrofit = apiModule.provideRetrofit(httpClient, moshi)
        Assert.assertNotNull(retrofit)
    }

    @Test
    fun testProvideLoggingInterceptor() {
        val loggingInterceptor = apiModule.provideLoggingInterceptor()
        Assert.assertNotNull(loggingInterceptor)
        assert(loggingInterceptor.level == HttpLoggingInterceptor.Level.BODY)
    }

    @Test
    fun testProvideHttpClient() {
        val cache = Cache(context.cacheDir, (10 * 1024 * 1024).toLong())
        val loggingInterceptor = apiModule.provideLoggingInterceptor()
        val httpClient = apiModule.provideHttpClient(cache, loggingInterceptor)
        Assert.assertNotNull(httpClient)
    }

    @Test
    fun testProvideCache() {

        val cache = apiModule.provideCache(context)
        Assert.assertNotNull(cache)
    }

    @Test
    fun testProvideMoshi() {
        val moshi = apiModule.provideMoshi()
        Assert.assertNotNull(moshi)
    }
}