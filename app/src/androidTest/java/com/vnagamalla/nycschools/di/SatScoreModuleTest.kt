package com.vnagamalla.nycschools.di

import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresApi
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresApiClient
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepo
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepoImpl
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject


@HiltAndroidTest
class SatScoreModuleTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var satScoreRepo: SatScoresRepo

    @Inject
    lateinit var satScoresApiClient: SatScoresApiClient

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testSatScoresRepoInjection() {
        Assert.assertNotNull(satScoreRepo)
        assert(satScoreRepo is SatScoresRepoImpl)
    }

    @Test
    fun testSatScoresApiClientInjection() {
        Assert.assertNotNull(satScoresApiClient)
    }

    @Test
    fun testSatScoresApiInjection() {
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }).build())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val satScoresApi = retrofit.create(SatScoresApi::class.java)
        Assert.assertNotNull(satScoresApi)
    }
}