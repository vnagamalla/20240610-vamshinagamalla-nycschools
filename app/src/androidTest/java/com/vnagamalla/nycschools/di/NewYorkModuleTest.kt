package com.vnagamalla.nycschools.di

import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresApi
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresApiClient
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepo
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepoImpl
import com.vnagamalla.nycschools.repo.nycschools.NewYorkApi
import com.vnagamalla.nycschools.repo.nycschools.NewYorkApiClient
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepo
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepoImpl
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject



@HiltAndroidTest
class NewYorkModuleTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var newYorkRepo: NewYorkRepo

    @Inject
    lateinit var newYorkApiClient: NewYorkApiClient

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun testNewYorkRepoInjection() {
        Assert.assertNotNull(newYorkRepo)
        assert(newYorkRepo is NewYorkRepoImpl)
    }

    @Test
    fun testNewYorkApiClientInjection() {
        Assert.assertNotNull(newYorkApiClient)
    }

    @Test
    fun testNewYorkApiInjection() {
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(OkHttpClient.Builder().addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }).build())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        val newYorkApi = retrofit.create(NewYorkApi::class.java)
        Assert.assertNotNull(newYorkApi)
    }
}