package com.vnagamalla.nycschools.ui.cityschools

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.data.School
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SchoolOptionTest (){

    private val school = School(
        token = "02M260",
        name = "Clinton School Writers & Artists, M.S. 260",
        description = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
        location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
        latitude = "40.73653",
        longitude = "-73.9927",
        phoneNumber = "212-524-4360",
        faxNumber = "212-524-4365",
        email = "admissions@theclintonschool.net",
        website = "www.theclintonschool.net",
        subway = "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
        bus = "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
        totalStudents = "376"
    )

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testSchoolOptionVisibleToUser(){
        composeTestRule.setContent {
            SchoolOptions(school = school)
        }
        composeTestRule.onNodeWithContentDescription("Phone").assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Email").assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Website").assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Share").assertIsDisplayed()
    }

    @Test
    fun testSchoolOptions_phone_click() {
        var phoneClicked = false

        composeTestRule.setContent {
            SchoolOptions(school = school)
        }

        composeTestRule.onNodeWithContentDescription("Phone").performClick()
        phoneClicked = true
        assert(phoneClicked)
    }

    @Test
    fun testSchoolOptions_website_click() {
        var websiteClicked = false

        composeTestRule.setContent {
            SchoolOptions(school = school)
        }

        composeTestRule.onNodeWithContentDescription("Website").performClick()
        websiteClicked = true
        assert(websiteClicked)
    }

    @Test
    fun testSchoolOptions_email_click() {
        var emailClicked = false

        composeTestRule.setContent {
            SchoolOptions(school = school)
        }

        composeTestRule.onNodeWithContentDescription("Email").performClick()
        emailClicked = true
        assert(emailClicked)
    }

    @Test
    fun testSchoolOptions_share_click() {

        var shareClicked = false

        composeTestRule.setContent {
            SchoolOptions(school = school)
        }

        composeTestRule.onNodeWithContentDescription("Share").performClick()
        shareClicked = true
        assert(shareClicked)
    }

}