package com.vnagamalla.nycschools.ui.detail

import androidx.activity.ComponentActivity
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.room.util.query
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.data.SatScores
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.ui.theme.NYCSchoolsTheme
import com.vnagamalla.nycschools.utils.Utils
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SatScoresBoxTest {

    private val satScores =  SatScores(
        token = "08X282",
        noOfSatTestTakers = "44",
        criticalReadingAverageScore = "407",
        mathAverageScore = "386",
        writingAverageScore = "378",
        schoolName = "WOMEN'S ACADEMY OF EXCELLENCE"
    )
    val school = School(
        token = "08X282",
        name = "Women's Academy of Excellence",
        description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!",
        location = "456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)",
        latitude = "40.81504",
        longitude = "-73.8561",
        phoneNumber = "718-542-0740",
        faxNumber = "718-542-0841",
        email = "sburns@schools.nyc.gov",
        website = "schools.nyc.gov/SchoolPortals/08/X282",
        subway = "N/A",
        bus = "Bx22, Bx27, Bx36, Bx39, Bx5",
        totalStudents = "338"
    )

    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Test
    fun testSatScoresBox(){
        composeTestRule.setContent {
            NYCSchoolsTheme {
                SatScoresBox(satScores = satScores , school = school)
            }
        }
        val percentage = school.totalStudents?.let {
            satScores.noOfSatTestTakers?.let { it1 ->
                Utils.calculatePercentage(
                    it1.toInt(),
                    it.toInt()
                )
            }
        }

        //check satscorebox displayed or not
        composeTestRule.onNodeWithTag("SatScores").assertIsDisplayed()
        //check percentage of test takers
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.percentage_of_test_takers,percentage)).assertIsDisplayed()
        //check critical reading
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.average_critical_reading_score,satScores.criticalReadingAverageScore)).assertIsDisplayed()
        //check average math score
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.average_math_score,satScores.mathAverageScore)).assertIsDisplayed()
        //check average writing score
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.average_writing_score,satScores.writingAverageScore)).assertIsDisplayed()
    }
}