package com.vnagamalla.nycschools.ui.profile

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.ui.cityschools.SchoolListTopAppBar
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class ProfileScreenTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testProfileScreen() {
        composeTestRule.setContent {
            ProfileScreen()
        }
        composeTestRule.onNodeWithText("Vamshi Nagamalla").assertIsDisplayed()

        val buttonNode = composeTestRule.onNodeWithText("Vamshi Nagamalla")
        buttonNode.performClick()

    }

    @Test
    fun testProfileTopAppBar(){
        composeTestRule.setContent {
            SchoolListTopAppBar(title = "Profile")
        }

        composeTestRule.onNodeWithText("Profile").assertIsDisplayed()
        composeTestRule.onNodeWithText("City School List").assertDoesNotExist()
    }







}
