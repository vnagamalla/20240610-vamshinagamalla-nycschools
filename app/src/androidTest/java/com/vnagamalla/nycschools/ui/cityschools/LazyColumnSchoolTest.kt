package com.vnagamalla.nycschools.ui.cityschools

import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithTag
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performScrollToIndex
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

object SchoolList{
     val schools = listOf(
        com.vnagamalla.nycschools.data.School(
            token = "02M260",
            name = "Clinton School Writers & Artists, M.S. 260",
            description = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
            location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
            latitude = "40.73653",
            longitude = "-73.9927",
            phoneNumber = "212-524-4360",
            faxNumber = "212-524-4365",
            email = "admissions@theclintonschool.net",
            website = "www.theclintonschool.net",
            subway = "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
            bus = "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
            totalStudents = "376"
        ),
        com.vnagamalla.nycschools.data.School(
            token = "08X282",
            name = "Women's Academy of Excellence",
            description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!",
            location = "456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)",
            latitude = "40.81504",
            longitude = "-73.8561",
            phoneNumber = "718-542-0740",
            faxNumber = "718-542-0841",
            email = "sburns@schools.nyc.gov",
            website = "schools.nyc.gov/SchoolPortals/08/X282",
            subway = "N/A",
            bus = "Bx22, Bx27, Bx36, Bx39, Bx5",
            totalStudents = "338"
        ),
        com.vnagamalla.nycschools.data.School(
            token = "02M260",
            name = "Clinton School Writers & Artists, M.S. 260",
            description = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
            location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
            latitude = "40.73653",
            longitude = "-73.9927",
            phoneNumber = "212-524-4360",
            faxNumber = "212-524-4365",
            email = "admissions@theclintonschool.net",
            website = "www.theclintonschool.net",
            subway = "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
            bus = "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
            totalStudents = "376"
        ),
        com.vnagamalla.nycschools.data.School(
            token = "08X282",
            name = "Women's Academy of Excellence",
            description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!",
            location = "456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)",
            latitude = "40.81504",
            longitude = "-73.8561",
            phoneNumber = "718-542-0740",
            faxNumber = "718-542-0841",
            email = "sburns@schools.nyc.gov",
            website = "schools.nyc.gov/SchoolPortals/08/X282",
            subway = "N/A",
            bus = "Bx22, Bx27, Bx36, Bx39, Bx5",
            totalStudents = "338"
        ),
        com.vnagamalla.nycschools.data.School(
            token = "02M260",
            name = "Clinton School Writers & Artists, M.S. 260",
            description = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
            location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
            latitude = "40.73653",
            longitude = "-73.9927",
            phoneNumber = "212-524-4360",
            faxNumber = "212-524-4365",
            email = "admissions@theclintonschool.net",
            website = "www.theclintonschool.net",
            subway = "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
            bus = "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
            totalStudents = "376"
        ),
        com.vnagamalla.nycschools.data.School(
            token = "08X282",
            name = "Women's Academy of Excellence",
            description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!",
            location = "456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)",
            latitude = "40.81504",
            longitude = "-73.8561",
            phoneNumber = "718-542-0740",
            faxNumber = "718-542-0841",
            email = "sburns@schools.nyc.gov",
            website = "schools.nyc.gov/SchoolPortals/08/X282",
            subway = "N/A",
            bus = "Bx22, Bx27, Bx36, Bx39, Bx5",
            totalStudents = "338"
        ),
         com.vnagamalla.nycschools.data.School(
             token = "02M260",
             name = "Clinton School Writers & Artists, M.S. 260",
             description = "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
             location = "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
             latitude = "40.73653",
             longitude = "-73.9927",
             phoneNumber = "212-524-4360",
             faxNumber = "212-524-4365",
             email = "admissions@theclintonschool.net",
             website = "www.theclintonschool.net",
             subway = "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St",
             bus = "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9",
             totalStudents = "376"
         ),
         com.vnagamalla.nycschools.data.School(
             token = "08X282",
             name = "Women's Academy of Excellence",
             description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!",
             location = "456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)",
             latitude = "40.81504",
             longitude = "-73.8561",
             phoneNumber = "718-542-0740",
             faxNumber = "718-542-0841",
             email = "sburns@schools.nyc.gov",
             website = "schools.nyc.gov/SchoolPortals/08/X282",
             subway = "N/A",
             bus = "Bx22, Bx27, Bx36, Bx39, Bx5",
             totalStudents = "338"
         ),


         )
}


@RunWith(AndroidJUnit4::class)
class LazyColumnSchoolTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    val schoolList = SchoolList.schools

    @Test
    fun testLazyColumnSchool(){
        composeTestRule.setContent {
            LazyColumnSchool(
                schools = SchoolList.schools,
            ) {
                assert(it == schoolList[2])

            }
        }
        composeTestRule.onNodeWithTag("SchoolList").performScrollToIndex(SchoolList.schools.size - 1)
//        // Verify the count of school items in the LazyColumn
//        composeTestRule.onAllNodesWithTag("SchoolList")
//            .assertCountEquals(8)

//        composeTestRule.onNodeWithTag("Menu Option").assertIsDisplayed()
//        composeTestRule.onNodeWithContentDescription("Phone").performClick()
    }

}