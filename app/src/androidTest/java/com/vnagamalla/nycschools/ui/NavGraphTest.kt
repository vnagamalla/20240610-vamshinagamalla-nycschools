package com.vnagamalla.nycschools.ui

import androidx.activity.compose.setContent
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.navigation.BottomNavScreen
import com.vnagamalla.nycschools.navigation.NavGraph
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class NavGraphTest1 {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeTestRule = createAndroidComposeRule<MainActivity>()


    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun testNavigationToHomeScreen() {
        composeTestRule.activity.setContent {
            val navController = rememberNavController()
            NavGraph(navHostController = navController)

            assert(navController.currentDestination?.route == BottomNavScreen.Home.route)
            // Simulate clicking on Profile navigation item
            navController.navigate(BottomNavScreen.Profile.route)

            // Verify that Profile screen is navigated to
            assert(navController.currentDestination?.route == BottomNavScreen.Profile.route)
            navController.navigate(BottomNavScreen.Home.route)
            assert(navController.currentDestination?.route == BottomNavScreen.Home.route)
        }

    }

    @Test
    fun testNavigateToProfileScreen(){
        composeTestRule.activity.setContent {
            val navController = rememberNavController()
            NavGraph(navHostController = navController)

            // Simulate clicking on Profile navigation item
            navController.navigate(BottomNavScreen.Profile.route)

            // Verify that Profile screen is navigated to
            assert(navController.currentDestination?.route == BottomNavScreen.Profile.route)
        }
    }
}