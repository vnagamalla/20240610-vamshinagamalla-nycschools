package com.vnagamalla.nycschools.ui.detail

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onAllNodesWithText
import androidx.compose.ui.test.onLast
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performScrollTo
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.R
import com.vnagamalla.nycschools.data.SatScores
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.repo.nycsatscores.SatScoresRepo
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepo
import com.vnagamalla.nycschools.ui.cityschools.FakeNewYorkRepo
import com.vnagamalla.nycschools.utils.Utils
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class SchoolDetailScreenTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    private lateinit var viewModel: SchoolDetailsViewModel


    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    @Before
    fun setup() = runBlocking{
        hiltRule.inject()
       delay(2000L)
        viewModel = SchoolDetailsViewModel(FakeNewYorkRepo(),FakeSatScoreRepo())
        delay(1000L)

    }

    @Test
    fun testSchoolDetailScreen() {
        val description = "The WomenÂ’s Academy of Excellence is an all-girls public high school, serving grades 9-12. Our mission is to create a community of lifelong learners, to nurture the intellectual curiosity and creativity of young women and to address their developmental needs. The school community cultivates dynamic, participatory learning, enabling students to achieve academic success at many levels, especially in the fields of math, science, and civic responsibility. Our scholars are exposed to a challenging curriculum that encourages them to achieve their goals while being empowered to become young women and leaders. Our Philosophy is GIRLS MATTER!"
        val address = Utils.removeBracesAndContents("456 White Plains Road, Bronx NY 10473 (40.815043, -73.85607)")

        composeTestRule.setContent {
            SchoolDetailsScreen(navHostController = rememberNavController(), viewModel = viewModel, token = "08X282")
        }
        composeTestRule.mainClock.advanceTimeBy(1000)
        composeTestRule.onAllNodesWithText("Women's Academy of Excellence")
            .assertCountEquals(2)
            .onLast().assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.about_this_school)).assertIsDisplayed()
        composeTestRule.onNodeWithText(description).assertIsDisplayed()
        composeTestRule.onNodeWithText(address)
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText("718-542-0740")
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.fax,"718-542-0841"))
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText("sburns@schools.nyc.gov")
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText("schools.nyc.gov/SchoolPortals/08/X282")
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.subway,"N/A"))
            .performScrollTo()
            .assertIsDisplayed()
        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.bus,"Bx22, Bx27, Bx36, Bx39, Bx5"))
            .performScrollTo()
            .assertIsDisplayed()

    }

    @Test
    fun testSchoolDetailScreenError(){

        viewModel = SchoolDetailsViewModel(FakeNewYorkRepo(),FakeEmptySatScoreRepo())
        composeTestRule.setContent {
            SchoolDetailsScreen(navHostController = rememberNavController(), viewModel,token = "08X282")
        }

        composeTestRule.onNodeWithText(composeTestRule.activity.getString(R.string.sat_scores_are_not_available_for_this_school)).assertIsDisplayed()

    }

}


class FakeSatScoreRepo : SatScoresRepo {

    private val satScores = listOf(
        SatScores(
            token = "08X282",
            noOfSatTestTakers = "44",
            criticalReadingAverageScore = "407",
            mathAverageScore = "386",
            writingAverageScore = "378",
            schoolName = "WOMEN'S ACADEMY OF EXCELLENCE"

        )
    )
    override suspend fun getSatScores(schoolToken: String): List<SatScores> {
        return satScores
    }



}
class FakeEmptySatScoreRepo : SatScoresRepo {

    private val satScores = emptyList<SatScores>()
    override suspend fun getSatScores(schoolToken: String): List<SatScores> {
        return satScores
    }



}

class FakeEmptyNewYorkRepo : NewYorkRepo {

    private val schools = emptyList<School>()

    override suspend fun getSchools(): List<School> {
        return schools
    }

    override suspend fun getSchool(token: String): School {
        return schools.filter { it.token == token }[0]
    }

    override suspend fun search(term: String): List<School> {
        return schools.filter { it.name?.contains(term, ignoreCase = true) ?: false }
    }
}