package com.vnagamalla.nycschools.ui.cityschools

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import com.vnagamalla.nycschools.R


@RunWith(AndroidJUnit4::class)
class MenuOptionTest  {


    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testMenuOption(){
        composeTestRule.setContent {
            MenuOption(
                icon = R.drawable.phone_outline_small,
                isEnabled = true,
                contentDescription = "Phone"
            ) {}
        }

        composeTestRule.onNodeWithTag("Menu Option").assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Phone").performClick()
    }

    @Test
    fun testMenuOptionIsClicked(){
        var clicked = false
        composeTestRule.setContent {
            MenuOption(
                icon = R.drawable.globe_small,
                isEnabled = true,
                contentDescription = "Phone"
            ) {
                clicked = true
            }
        }

        composeTestRule.onNodeWithContentDescription("Phone").performClick()
        assert(clicked)

    }
}