package com.vnagamalla.nycschools.ui.cityschools

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.assertIsNotDisplayed
import androidx.compose.ui.test.assertTextEquals
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.vnagamalla.nycschools.ui.CustomSearchField
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CustomSearchFieldTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun testSearchFieldIsDisplayed() {
        composeTestRule.setContent {
            CustomSearchField(search = {})
        }

        // Verify the search field is displayed
        composeTestRule.onNodeWithTag("Search").assertIsDisplayed()
    }

    @Test
    fun testSearchFunctionIsCalled() {
        var searchText = ""
        composeTestRule.setContent {
            CustomSearchField(search = { searchText = it })
        }

        // Enter text in the search field
        composeTestRule.onNodeWithTag("Search").performTextInput("Test")

        // Verify the search function is called with the correct text
        assert(searchText == "Test")
    }

    @Test
    fun testClearIconClearsText() {
        var searchText = ""
        composeTestRule.setContent {
            CustomSearchField(search = { searchText = it })
        }

        //Verify the placeholder is displayed
        composeTestRule.onNodeWithText("Search").assertIsDisplayed()

        // Enter text in the search field
        composeTestRule.onNodeWithTag("Search").performTextInput("Test")
        composeTestRule.onNodeWithTag("Search").assertTextEquals("Test")

        //Verify the placeholder is not displayed
        composeTestRule.onNodeWithText("Search").assertDoesNotExist()

        // Verify the clear icon is displayed
        composeTestRule.onNodeWithContentDescription("Clear").assertIsDisplayed()

        // Click the clear icon
        composeTestRule.onNodeWithContentDescription("Clear").performClick()

      //  composeTestRule.onNodeWithContentDescription("Clear").assertIsNotDisplayed()

        // Verify the text is cleared
        composeTestRule.onNodeWithTag("Search").assert(hasText(""))
        assert(searchText == "")
    }
}