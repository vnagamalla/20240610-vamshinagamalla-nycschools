package com.vnagamalla.nycschools.ui

import androidx.activity.compose.setContent
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onAllNodesWithText
import androidx.compose.ui.test.onFirst
import androidx.compose.ui.test.onNodeWithContentDescription
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performScrollToIndex
import androidx.navigation.compose.rememberNavController
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith



@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class MainScreenTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeTestRule = createAndroidComposeRule<MainActivity>()


    @Before
    fun setup() {
        hiltRule.inject()
    }

    @Test
    fun testNavigationToSchoolDetails() {
        composeTestRule.activity.setContent {
            val navController = rememberNavController()
            MainScreen(navController = navController)
        }

        composeTestRule.onNodeWithTag("Loading").assertIsDisplayed()
        composeTestRule.onNodeWithText("Not Loading").assertDoesNotExist()


        composeTestRule.waitUntil(10000L){
            composeTestRule.onNodeWithText("City School List").assertIsDisplayed()
            composeTestRule
                .onAllNodesWithText("Clinton School Writers & Artists, M.S. 260")
                .fetchSemanticsNodes().size == 1

        }

        composeTestRule.onNodeWithTag("SchoolList").performScrollToIndex(0).performClick()
        composeTestRule.onNodeWithTag("DetailScreen").assertIsDisplayed()
        composeTestRule.onAllNodesWithText("Liberation Diploma Plus High School")
            .assertCountEquals(2)
            .onFirst().assertIsDisplayed()
        composeTestRule.onNodeWithContentDescription("Back Press").performClick()
    }
}