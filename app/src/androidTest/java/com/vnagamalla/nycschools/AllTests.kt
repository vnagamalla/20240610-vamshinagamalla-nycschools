package com.vnagamalla.nycschools

import com.vnagamalla.nycschools.database.CustomDatabase
import com.vnagamalla.nycschools.di.ApiModuleTest
import com.vnagamalla.nycschools.di.NewYorkModuleTest
import com.vnagamalla.nycschools.di.SatScoreModuleTest
import com.vnagamalla.nycschools.ui.cityschools.CitySchoolListScreenTest
import com.vnagamalla.nycschools.ui.cityschools.CustomSearchFieldTest
import com.vnagamalla.nycschools.ui.cityschools.LazyColumnSchoolTest
import com.vnagamalla.nycschools.ui.cityschools.MenuOptionTest
import com.vnagamalla.nycschools.ui.cityschools.SchoolOptionTest
import com.vnagamalla.nycschools.ui.cityschools.SchoolTest
import com.vnagamalla.nycschools.ui.detail.SatScoresBoxTest
import com.vnagamalla.nycschools.ui.detail.SchoolDetailScreenTest
import com.vnagamalla.nycschools.ui.profile.ProfileScreenTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(Suite::class)
@Suite.SuiteClasses(
    CustomDatabase::class,
    ApiModuleTest::class,
    NewYorkModuleTest::class,
    SatScoreModuleTest::class,
    CitySchoolListScreenTest::class,
    CustomSearchFieldTest::class,
    LazyColumnSchoolTest::class,
    MenuOptionTest::class,
    SchoolOptionTest::class,
    SchoolTest::class,
    SchoolDetailScreenTest::class,
    SatScoresBoxTest::class,
    ProfileScreenTest::class,

)
class AllTests