package com.vnagamalla.nycschools

import com.vnagamalla.nycschools.ui.UtilsTest
import com.vnagamalla.nycschools.ui.cityschools.CitySchoolViewModelTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith
import org.junit.runners.Suite

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(Suite::class)
@Suite.SuiteClasses(
    CitySchoolViewModelTest::class,
    UtilsTest::class,
    )
class AllTests