package com.vnagamalla.nycschools

import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException
import java.nio.charset.Charset


/**
 * All the json resources are placed in the test/resources package. This helper converts the .json
 * files into strings for use in the tests.
 * */
object Utils {

    /**
     * Use the calling class loader to access resources and convert the file into a string
     */
    fun readFileFromResources(fileName: String?): String? {
        try {
            val file = javaClass.classLoader?.getResource(fileName)?.path?.let { File(it) }
            return FileUtils.readFileToString(file, Charset.defaultCharset())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return ""
    }
}