package com.vnagamalla.nycschools.ui

import com.vnagamalla.nycschools.utils.Utils
import org.junit.Assert.assertEquals
import org.junit.Test

class UtilsTest {

    @Test
    fun testRemoveBracesAndContents() {
        val stringWithBraces = "This is a (test) string"
        val expectedResult = "This is a  string"
        val result = Utils.removeBracesAndContents(stringWithBraces)
        assertEquals(expectedResult, result)
    }

    @Test
    fun testCalculatePercentage() {
        val value = 75
        val total = 100
        val expectedResult = "75.00"
        val result = Utils.calculatePercentage(value, total)
        assertEquals(expectedResult, result)
    }
}