package com.vnagamalla.nycschools.ui.cityschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.squareup.moshi.Moshi
import com.vnagamalla.nycschools.Utils
import com.vnagamalla.nycschools.data.School
import com.vnagamalla.nycschools.extensions.jsonToObject
import com.vnagamalla.nycschools.repo.nycschools.NewYorkRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class CitySchoolViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = StandardTestDispatcher()

    @Mock
    private lateinit var newYorkRepo: NewYorkRepo

    private lateinit var viewModel: CitySchoolViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun successfulRetrievalOfSchools() = runBlocking {
        val json = Utils.readFileFromResources("schools.json")

        var schools: List<School>? = null

        try {
            json?.let { first ->
                schools = Moshi.Builder().build().jsonToObject<List<School>>(first)
                schools?.let {
                    Mockito.`when`(newYorkRepo.getSchools()).thenReturn(schools)
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        viewModel = CitySchoolViewModel(newYorkRepo)
        testDispatcher.scheduler.advanceUntilIdle()

        Mockito.verify(newYorkRepo).getSchools()
        assert(viewModel.schoolList.value == schools)
    }
}