import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    kotlin("kapt")
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.dagger.hilt.android")
    id("com.google.android.libraries.mapsplatform.secrets-gradle-plugin")
}

android {
    namespace = "com.vnagamalla.nycschools"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.vnagamalla.nycschools"
        minSdk = 26
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunner = "com.vnagamalla.nycschools.di.CustomTestRunner"
        vectorDrawables {
            useSupportLibrary = true
        }

        javaCompileOptions {
            annotationProcessorOptions {
                argument("room.schemaLocation", "$projectDir/schemas")
            }
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }

        debug {
            isMinifyEnabled = false
            isDebuggable = true

            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
        buildConfig = true
        viewBinding = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.5.1"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    //for submission purpose, placing the key directly here, this should in local properties though
    val mapsApiKey = "AIzaSyASCvMu7JfVbiqH8eRe2_4nVJUEkSRAJoI"
    //val mapsApiKey = gradleLocalProperties(rootDir).getProperty("mapsApiKey")


    applicationVariants.all {
        buildConfigField("String", "API", "\"https://data.cityofnewyork.us\"")
        resValue("string", "maps_api_key", mapsApiKey)
    }
}

kapt {
    correctErrorTypes = true
}

dependencies {

    //region androidx
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.7.0")
    implementation("androidx.activity:activity-compose:1.8.2")
    implementation(platform("androidx.compose:compose-bom:2023.10.01"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.runtime:runtime-livedata:1.5.4")
    implementation("androidx.navigation:navigation-compose:2.7.6")
    //endregion androidx

    //region hilt
    implementation("com.google.dagger:hilt-android:2.50")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.7.0")
    implementation("androidx.test.ext:junit-ktx:1.1.5")
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
    kapt("com.google.dagger:hilt-android-compiler:2.50")
    implementation("androidx.hilt:hilt-navigation-compose:1.2.0")
    //endregion hilt

    //region room
    val roomVersion = "2.6.1"
    implementation("androidx.room:room-runtime:$roomVersion")
    annotationProcessor("androidx.room:room-compiler:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")
    //endregion room

    //region networking
    //add legal text if needed as per Retrofit licence
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    //add legal text if needed as per OkHttp licence
    implementation("com.squareup.okhttp3:okhttp:4.12.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.12.0")
    //add legal text if needed as per Moshi licence
    implementation("com.squareup.moshi:moshi-kotlin:1.15.0")
    //add legal text if needed as per retrofit licence
    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
    //endregion networking

    //region logging
    //add legal text if needed as per Timber licence
    implementation("com.jakewharton.timber:timber:5.0.1")
    //endregion logging

    //region maps
    implementation("com.google.maps.android:maps-compose:4.3.0")
    implementation("com.google.android.gms:play-services-maps:18.2.0")
    //endregion maps

    implementation("commons-io:commons-io:2.15.1")

    //region tests
    testImplementation("junit:junit:4.13.2")
    testImplementation("org.mockito:mockito-core:3.3.0")
    testImplementation("io.mockk:mockk:1.13.9")
    testImplementation("androidx.arch.core:core-testing:2.2.0")
    testImplementation("org.robolectric:robolectric:4.4")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.7.1")

    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2023.10.01"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")
    //endregion tests

    // For Robolectric tests.
    testImplementation("com.google.dagger:hilt-android-testing:2.50")
    // ...with Kotlin.
    kaptTest("com.google.dagger:hilt-android-compiler:2.50")
    // For instrumented tests.
    androidTestImplementation("com.google.dagger:hilt-android-testing:2.50")
    // ...with Kotlin.
    kaptAndroidTest("com.google.dagger:hilt-android-compiler:2.50")
    androidTestImplementation ("androidx.navigation:navigation-testing:2.5.3")
    testImplementation ("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")
    testImplementation ("org.mockito.kotlin:mockito-kotlin:4.1.0")
    testImplementation ("io.mockk:mockk:1.13.9")
    testImplementation ("org.mockito:mockito-core:3.11.2")
    androidTestImplementation ("org.mockito:mockito-android:3.11.2")
    androidTestImplementation("com.squareup.okhttp3:mockwebserver:4.9.1")


}