- Displays the list of NYC high schools.
- Selecting a school will show additional information about the school.

## Technologies Used

- **Kotlin**: The primary programming language used for developing the application.
- **Jetpack Compose**: Used for building the modern, declarative UI of the application.
- **Hilt**: Used for dependency injection to manage the app's dependencies.
- **Coroutines**: Used for asynchronous programming and handling background tasks.
- **Retrofit**: Used for making network requests and fetching data from the Flickr API.
- **Coil**: Used for image loading and caching.
- **Mockito**: Used for mocking objects in unit tests.
- **Espresso**: Used for UI testing.
